import feature_img1 from "../../assets/images/service-01.png";
import feature_img2 from "../../assets/images/service-02.png";
import feature_img3 from "../../assets/images/service-03.png";

export interface IFeature {
  title: string;
  imgUrl: string;
  desc: string;
}

export const FEATURE_DATA = [
  {
    title: "Quick Delivery",
    imgUrl: feature_img1,
    desc: "Lorem ipsum dolor sit amet consectso adipisicia elit miquam dolore",
  },
  {
    title: "Super Dine In",
    imgUrl: feature_img2,
    desc: "Lorem ipsum dolor sit amet consectso adipisicia elit miquam dolore",
  },
  {
    title: "Easy Pick Up",
    imgUrl: feature_img3,
    desc: "Lorem ipsum dolor sit amet consectso adipisicia elit miquam dolore",
  },
];
