export interface CommonCartPayload {
  id: number;
  title: string;
  image01: string;
  price: number;
  quantity: number;
  totalPrice: number;
}
