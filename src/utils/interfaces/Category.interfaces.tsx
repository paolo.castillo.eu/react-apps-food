export const ICategory: { [key: string]: string } = {
  BURGER: "Burger",
  PIZZA: "Pizza",
  BREAD: "Bread",
};
