// class Item {
//   id: number;
//   title: string;
//   image01: string;
//   price: number;
//   quantity: number;
//   totalPrice: number;

//   constructor(
//     id: number,
//     title: string,
//     image01: string,
//     price: number,
//     quantity: number,
//     totalPrice: number
//   ) {
//     this.id = id;
//     this.title = title;
//     this.image01 = image01;
//     this.price = price;
//     this.quantity = quantity;
//     this.totalPrice = totalPrice;
//   }
// }
export interface IItem {
  id: string;
  title: string;
  image01: string;
  price: number;
  quantity: number;
  totalPrice: number;
}

export interface ICart {
  items: IItem[];
  totalQuantity: number;
  totalAmount: number;
}
