import food_category_img1 from "../../assets/images/hamburger.png";
import food_category_img2 from "../../assets/images/pizza.png";
import food_category_img3 from "../../assets/images/bread.png";

export interface IPopular {
  title?: string;
  category: string;
  className: string;
  imgUrl?: string;
}

export const POPULAR_DATA: IPopular[] = [
  {
    title: "ALL",
    category: "ALL",
    className: "all_btn",
    imgUrl: "",
  },
  {
    title: "Burger",
    category: "BURGER",
    className: "d-flex align-items-center gap-2",
    imgUrl: food_category_img1,
  },
  {
    title: "Pizza",
    category: "PIZZA",
    className: "d-flex align-items-center gap-2",
    imgUrl: food_category_img2,
  },
  {
    title: "Bread",
    category: "BREAD",
    className: "d-flex align-items-center gap-2",
    imgUrl: food_category_img3,
  },
];
