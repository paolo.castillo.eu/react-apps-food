import React from "react";

export interface IRouter {
  path: string;
  name?: string;
  component: React.LazyExoticComponent<() => JSX.Element>;
  childRoutes?: IRouter[];
}
