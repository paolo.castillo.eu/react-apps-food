export interface Shipping {
  name: string;
  email: string;
  phone: number;
  country: string;
  city: string;
  postal: string;
}
