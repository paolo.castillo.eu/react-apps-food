import { configureStore } from "@reduxjs/toolkit";
import cartSlice from "./shopping-cart/Cart.slice";
import cartUISlice from "./shopping-cart/CartUI.slice";

const Store = configureStore({
  reducer: {
    cart: cartSlice.reducer,
    cartUI: cartUISlice.reducer,
  },
});
// NOTE redux toolkit
// Here configureStore is a friendly abstraction over the standard Redux createStore function that adds
// good defaults to the store setup for a better development experience. It helps us to combine different
// educers without using the combineReducers package.
export type RootCartState = ReturnType<typeof Store.getState>;
export type AppCartDispatch = typeof Store.dispatch;
export default Store;
