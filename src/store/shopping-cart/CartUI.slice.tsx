import { createSlice } from "@reduxjs/toolkit";
import { ICartUI } from "../../utils/interfaces/CartUI.interfaces";
const cartUISlice = createSlice({
  name: "cartUI",
  initialState: { cartIsVisible: false },
  reducers: {
    toggle(state: ICartUI) {
      state.cartIsVisible = !state.cartIsVisible;
    },
  },
});

export default cartUISlice;
