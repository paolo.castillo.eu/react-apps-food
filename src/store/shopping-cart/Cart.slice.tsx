import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICart, IItem } from "../../utils/interfaces/Cart.interfaces";

const initialState: ICart = {
  items: [], // all items
  totalQuantity: 0,
  totalAmount: 0,
};

const cartSlice = createSlice({
  name: "cart",
  initialState: initialState,
  reducers: {
    addItem(state: ICart, action: PayloadAction<IItem>) {
      const newItem = action.payload;
      const existingItem = state.items.find((item) => item.id === newItem.id);
      state.totalQuantity++;
      if (!existingItem) {
        // NOTE reducers items
        // if you use just redux you should not mute state array instead of clone the state array,
        // but if you use redux toolkit that will not a problem because redux toolkit clone
        // array behind the scene
        console.log(
          `${newItem.quantity} ${newItem.price} ${newItem.totalPrice}`
        );
        state.items.push({
          id: newItem.id,
          title: newItem.title,
          image01: newItem.image01,
          price: newItem.price,
          quantity: newItem.quantity,
          totalPrice: newItem.price,
        });
      } else {
        existingItem.quantity++;
        existingItem.totalPrice = calculateTotalPrice(
          existingItem.price,
          existingItem.quantity
        );
      }
      state.totalAmount = calculateTotalAmount(state.items);
    },
    removeItem(state: ICart, action: PayloadAction<string>) {
      const id = action.payload;
      const existingItem = state.items.find((item) => item.id === id);
      state.totalQuantity--;
      if (existingItem && existingItem.quantity > 1) {
        existingItem.quantity--;
        existingItem.totalPrice = calculateTotalPrice(
          existingItem.price,
          existingItem.quantity
        );
        state.totalAmount = calculateTotalAmount(state.items);
      } else {
        state.items = state.items.filter((item) => item.id !== id);
      }
    },
    deleteItem(state: ICart, action: PayloadAction<string>) {
      const id = action.payload;
      const existingItem = state.items.find((item) => item.id === id);
      if (existingItem) {
        state.items = state.items.filter((item) => item.id !== id);
        state.totalQuantity = state.totalQuantity - existingItem.quantity;
        state.totalAmount = calculateTotalAmount(state.items);
      }
    },
  },
});

const calculateTotalPrice = (pricer: number, quantity: number): number =>
  pricer * quantity;

const calculateTotalAmount = (item: IItem[]): number =>
  item
    .map((item) => item.price * item.quantity)
    .reduce((total, item) => total + item, 0);

export default cartSlice;
