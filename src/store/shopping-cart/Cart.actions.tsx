import cartSlice from "./Cart.slice";
import cartUISlice from "./CartUI.slice";

export const cartActions = cartSlice.actions;
export const cartUIActions = cartUISlice.actions;
