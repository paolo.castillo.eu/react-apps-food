/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Link } from "react-router-dom";
import { Col, Container, Row, ListGroup, ListGroupItem } from "reactstrap";
import logo from "../../assets/images/res-logo.png";
import "../../styles/footer.css";

const Footer = (): JSX.Element => {
  return (
    <footer className="footer">
      <Container>
        <Row>
          <Col lg="3" md="4" sm="6">
            <div className="footer__logo text-start">
              <img src={logo} alt="logo" />
              <h5>Tasty Treat</h5>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt,
                porro eos quod eum, itaque quibusdam ex magnam dolores
                consequuntur blanditiis unde dolorem natus, veniam animi ipsa
                deserunt? Saepe, tempora delectus?
              </p>
            </div>
          </Col>
          <Col lg="3" md="4" sm="6">
            <h5 className="footer__title">Delivery items</h5>
            <ListGroup className="deliver__time-list">
              <ListGroupItem className="delivery__time-items border-0 ps-0">
                <span>Sunday - Thrusday</span>
                <p>10:00am - 11:00pm</p>
              </ListGroupItem>
              <ListGroupItem className="delivery__time-items border-0 ps-0">
                <span>Friday - Saturday</span>
                <p>Off day - Off day</p>
              </ListGroupItem>
            </ListGroup>
          </Col>
          <Col lg="3" md="4" sm="6">
            <h5 className="footer__title">Contact</h5>
            <ListGroup className="deliver__time-list">
              <ListGroupItem className="delivery__time-items border-0 ps-0">
                <span>Location: clSantiago, Av. Macul 1045</span>
              </ListGroupItem>
              <ListGroupItem className="delivery__time-items border-0 ps-0">
                <span>Phone: 01712345678</span>
              </ListGroupItem>
              <ListGroupItem className="delivery__time-items border-0 ps-0">
                <span>Email: example@gmail.com</span>
              </ListGroupItem>
            </ListGroup>
          </Col>
          <Col lg="3" md="4" sm="6">
            <h5 className="footer__title">Newletter</h5>
            <div className="newletter">
              <input type="email" placeholder="Enter your email" />
              <span>
                <i className="ri-send-plane-line"></i>
              </span>
            </div>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col lg="6" md="6">
            <p className="copyright__text">
              Copyright - 2022, website made Paolo, All Rights Reserved.
            </p>
          </Col>
          <Col lg="6" md="6">
            <div className="social__link d-flex align-items-center gap-4 justify-content-end">
              <p className="m-0">Follow: </p>
              <span>
                <Link to="https://www.facebook.com/">
                  <i className="ri-facebook-fill"></i>
                </Link>
              </span>
              <span>
                <Link to="https://www.gitlab.com/">
                  <i className="ri-gitlab-fill"></i>
                </Link>
              </span>
              <span>
                <Link to="https://www.youtube.com/">
                  <i className="ri-youtube-fill"></i>
                </Link>
              </span>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
