import { ListGroupItem } from "reactstrap";
import { useAppDispatch } from "../../../hook/Cart.hooks";
import { cartActions } from "../../../store/shopping-cart/Cart.actions";
import "../../../styles/cart-item.css";
import { IItem } from "../../../utils/interfaces/Cart.interfaces";
export const CartItem: React.FC<IItem> = (item: IItem): JSX.Element => {
  const dispatch = useAppDispatch();
  const incrementItem = () => {
    dispatch(
      cartActions.addItem({
        id: item.id,
        title: item.title,
        image01: item.image01,
        price: item.price,
        quantity: item.quantity,
        totalPrice: item.totalPrice,
      })
    );
  };
  const descrece = () => dispatch(cartActions.removeItem(item.id));
  const deleteItem = () => dispatch(cartActions.deleteItem(item.id));
  return (
    <ListGroupItem className="border-0 cart__item">
      <div className="cart__item-info d-flex gap-2">
        <img src={item.image01} alt="" />
        <div className="cart__product-info w-100 d-flex align-content-center justify-content-between">
          <div>
            <h6 className="cart__product-title">{item.title}</h6>
            <p className="d-flex align-content-center gap-5 cart__product-price">
              {item.quantity}x <span>${item.totalPrice}</span>
            </p>
            <div className="d-flex align-content-center justify-content-between gap-3 increse__descrece-btn">
              <span className="increase__btn" onClick={incrementItem}>
                <i className="ri-add-line"></i>
              </span>
              <span className="quantity">{item.quantity}</span>
              <span className="descrece__btn" onClick={descrece}>
                <i className="ri-subtract-line"></i>
              </span>
            </div>
          </div>
          <span className="delete__btn" onClick={deleteItem}>
            <i className="ri-close-fill"></i>
          </span>
        </div>
      </div>
    </ListGroupItem>
  );
};
