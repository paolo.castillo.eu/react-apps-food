import React from "react";
import { ListGroup } from "reactstrap";
import { CartItem } from "./CartItem";
import { Link } from "react-router-dom";
import "../../../styles/shooping-cart.css";
import { useAppDispatch, useAppSelector } from "../../../hook/Cart.hooks";
import { cartUIActions } from "../../../store/shopping-cart/Cart.actions";

export const Cart = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const cartProducts = useAppSelector((state) => state.cart.items);
  const totalAmount = useAppSelector((state) => state.cart.totalAmount);
  const toggleCart = () => {
    dispatch(cartUIActions.toggle());
  };
  return (
    <div className="cart__container">
      <ListGroup className="cart">
        <div className="cart__close">
          <span onClick={toggleCart}>
            <i className="ri-close-line"></i>
          </span>
        </div>
        <div className="cart__item-list">
          {cartProducts.length === 0 ? (
            <h6 className="text-center mt-5">No item to the cart</h6>
          ) : (
            cartProducts.map((item, index) => (
              <CartItem key={index} {...item} />
            ))
          )}
        </div>
        <div className="cart__bottom d-flex align-content-center justify-content-between">
          <h6>
            Sub total amount: <span>${totalAmount}</span>
          </h6>
          <button>
            <Link to={"/checkout"}>Checkout</Link>
          </button>
        </div>
      </ListGroup>
    </div>
  );
};
