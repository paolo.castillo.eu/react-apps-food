import React from "react";
import { Container } from "reactstrap";
import "../../../styles/common-section.css";

interface Props {
  title: string;
}

export const CommonSection: React.FC<Props> = (props: Props): JSX.Element => {
  return (
    <section className="common__section">
      <Container>
        <h2 className="text-white">{props.title}</h2>
      </Container>
    </section>
  );
};
