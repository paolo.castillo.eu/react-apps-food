import React from "react";
interface Props {
  children: React.ReactNode;
  type?: "submit" | "button";
  className?: string;
  onClick?: () => void;
}
export const Button: React.FC<Props> = (props: Props): JSX.Element => {
  return (
    <button
      type={props.type}
      className={props.className}
      {...(props.onClick && { onClick: props.onClick })}
    >
      {props.children}
    </button>
  );
};
