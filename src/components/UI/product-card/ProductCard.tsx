import { Link } from "react-router-dom";
import { useAppDispatch } from "../../../hook/Cart.hooks";
import { cartActions } from "../../../store/shopping-cart/Cart.actions";
import "../../../styles/product-car.css";

interface Props {
  item: {
    id: string;
    title: string;
    price: number;
    image01: string;
    image02: string;
    image03: string;
    category: string;
    desc: string;
  };
}

export const ProductCard: React.FC<Props> = ({ item: Props }): JSX.Element => {
  const { id, title, price, image01, image02, image03, category, desc } = Props;
  const dispatch = useAppDispatch();
  const addToCard = () => {
    dispatch(
      cartActions.addItem({
        id,
        title,
        image01,
        price,
        quantity: 1,
        totalPrice: 0,
      })
    );
  };
  return (
    <div className="product__item">
      <div className="product__img">
        <img src={image01} alt={title} className="w-50" />
      </div>
      <div className="product__content">
        <h5>
          <Link to={`/foods/${id}`}>{title}</Link>
        </h5>
        <div className="d-flex align-content-center justify-content-between">
          <span className="product__price">$ {price}</span>
          <button className="addTOCart__btn" onClick={addToCard}>
            Add to Card
          </button>
        </div>
      </div>
    </div>
  );
};
