import { Container, Col, Row } from "reactstrap";
import "../../../styles/category.css";
import category_img1 from "../../../assets/images/category-01.png";
import category_img2 from "../../../assets/images/category-02.png";
import category_img3 from "../../../assets/images/category-03.png";
import category_img4 from "../../../assets/images/category-04.png";

const CATEGORY_DATA = [
  {
    display: "Fast food",
    imgUrl: category_img1,
  },
  {
    display: "Pizza",
    imgUrl: category_img2,
  },
  {
    display: "Asian Food",
    imgUrl: category_img3,
  },
  {
    display: "Row Meat",
    imgUrl: category_img4,
  },
];

export const Category = (): JSX.Element => {
  return (
    <Container>
      <Row>
        {CATEGORY_DATA.map((item, index) => (
          <Col lg="3" md="4" sm="6" xs="6" key={index} className="mb-3">
            <div className="category__item d-flex align-content-center gap-3">
              <div className="category__img">
                <img src={item.imgUrl} alt="category__item" />
              </div>
              <h6>{item.display}</h6>
            </div>
          </Col>
        ))}
      </Row>
    </Container>
  );
};
