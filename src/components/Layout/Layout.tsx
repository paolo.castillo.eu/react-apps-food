import { useAppSelector } from "../../hook/Cart.hooks";
import Routers from "../../routes/Router";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import { Cart } from "../UI/card/Cart";

const Layout = (): JSX.Element => {
  const showCart = useAppSelector((state) => state.cartUI.cartIsVisible);
  return (
    <>
      <Header />
      {showCart && <Cart />}
      <div>
        <Routers />
      </div>
      <Footer />
    </>
  );
};

export default Layout;
