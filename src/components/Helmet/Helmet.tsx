import React from "react";
interface Props {
  children: React.ReactNode; // multiple children
  // children: JSX.Element;
  title: string;
}

const Helmet: React.FC<Props> = (props: Props): JSX.Element => {
  document.title = "Foood ordering apps -" + props.title;
  return <div className="w-100">{props.children}</div>;
};

export default Helmet;
