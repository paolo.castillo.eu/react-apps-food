import { TypedUseSelectorHook, useSelector, useDispatch } from "react-redux";
import { AppCartDispatch, RootCartState } from "../store";

export const useAppSelector: TypedUseSelectorHook<RootCartState> = useSelector;
export const useAppDispatch = () => useDispatch<AppCartDispatch>();
