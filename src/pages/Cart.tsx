import React from "react";
import { Link } from "react-router-dom";
import Helmet from "../components/Helmet/Helmet";
import { CommonSection } from "../components/UI/common-section/CommonSection";
import { Container, Row, Col } from "reactstrap";
import { IItem } from "../utils/interfaces/Cart.interfaces";
import { useAppSelector, useAppDispatch } from "../hook/Cart.hooks";
import { cartActions } from "../store/shopping-cart/Cart.actions";
import "../styles/cart-page.css";
import "../styles/product-car.css";

const Cart = (): JSX.Element => {
  const items = useAppSelector((state) => state.cart.items);
  const totalAmount = useAppSelector((state) => state.cart.totalAmount);
  const totalQuantity = useAppSelector((state) => state.cart.totalQuantity);
  return (
    <Helmet title="Cart">
      <CommonSection title="Your Cart" />
      <section>
        <Container>
          <Row>
            <Col lg="12">
              {items.length === 0 ? (
                <h5 className="text-center mb-5">Your cart is empty</h5>
              ) : (
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th>Image</th>
                      <th>Product title</th>
                      <th>Price</th>
                      <th>Quantity</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {items.map((item) => (
                      <Tr {...item} key={item.id} />
                    ))}
                  </tbody>
                  <tfoot></tfoot>
                </table>
              )}
              <div className="mt-4">
                <h6>
                  totalQuantity:${" "}
                  <span className="cart__subtotal">{totalQuantity}</span>
                </h6>
                <h6>
                  SubTotal: <span>${totalAmount}</span>
                </h6>
                <p>Taxes and shipping will calculate at checkout</p>
                <div className="cart__page-btn">
                  <button className="addTOCart__btn me-4">
                    <Link to={"/foods"}> Continue Shopping</Link>
                  </button>
                  <button className="addTOCart__btn">
                    <Link to={"/checkout"}> Proceed to checkout</Link>
                  </button>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  );
};

const Tr: React.FC<IItem> = (props: IItem): JSX.Element => {
  const { image01, title, price, quantity } = props;
  const dispatch = useAppDispatch();
  const deleteItem = () => {
    dispatch(cartActions.removeItem);
  };
  return (
    <tr>
      <td className="text-center cart__img-box">
        <img src={image01} alt={title} />
      </td>
      <td className="text-center">{title}</td>
      <td className="text-center">${price * quantity}</td>
      <td className="text-center">{quantity}px</td>
      <td className="text-center cart__item-del">
        <i className="ri-delete-bin-line" onClick={deleteItem}></i>
      </td>
    </tr>
  );
};

export default Cart;
