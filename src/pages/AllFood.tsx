import { Col, Container, Row } from "reactstrap";
import ReactPaginate from "react-paginate";
import { CommonSection } from "../components/UI/common-section/CommonSection";
import Helmet from "../components/Helmet/Helmet";
import Products from "../assets/fake-data/Products";
import { ProductCard } from "../components/UI/product-card/ProductCard";
import { useState } from "react";

import "../styles/all-foods.css";
import "../styles/pagination.css";

const AllFood = (): JSX.Element => {
  const [searchItem, setSearchItem] = useState<string>("");
  const [pageNumber, setPageNumber] = useState<number>(0);

  const searchedProduct = Products.filter((item) =>
    searchItem === ""
      ? item
      : item.title.toLowerCase().includes(searchItem.toLowerCase())
  );

  const productPerPage = 8;
  const visitedPage = pageNumber * productPerPage;
  const displayPage = searchedProduct.slice(
    visitedPage,
    visitedPage + productPerPage
  );

  const pageCount = Math.ceil(searchedProduct.length / productPerPage);
  const changePage = (selectedItem: { selected: number }) => {
    setPageNumber(selectedItem.selected);
  };
  return (
    <Helmet title="All Foods">
      <CommonSection title="All Foods" />
      <section>
        <Container>
          <Row>
            <Col lg="6" md="6" sm="6" xs="12" className="mb-5">
              <div className="search__widget d-flex align-items-center justify-content-between w-50">
                <input
                  type="text"
                  placeholder="I`m looking for ..."
                  value={searchItem}
                  onChange={(e) => setSearchItem(e.target.value)}
                />
                <span>
                  <i className="ri-search-line"></i>
                </span>
              </div>
            </Col>
            <Col lg="6" md="6" sm="6" className="mb-5">
              <div className="sorting__widget text-end">
                <select className="w-50">
                  <option value="">Default</option>
                  <option value="ascending">Alphabetically, A-Z</option>
                  <option value="decending">Alphabetically, Z-A</option>
                  <option value="high-price">High Price</option>
                  <option value="low-price">Low Price</option>
                </select>
              </div>
            </Col>
            {displayPage.map((item) => (
              <Col lg="3" md="4" sm="6" xs="6" key={item.id} className="mb-4">
                <ProductCard item={item} />
              </Col>
            ))}

            <div>
              <ReactPaginate
                pageCount={pageCount}
                onPageChange={changePage}
                previousLabel="Next"
                containerClassName="paginationBttns"
              />
            </div>
          </Row>
        </Container>
      </section>
    </Helmet>
  );
};

export default AllFood;
