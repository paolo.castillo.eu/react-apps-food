import React, { useState } from "react";
import { Container, Row, Col } from "reactstrap";
import Helmet from "../components/Helmet/Helmet";
import { useAppSelector } from "../hook/Cart.hooks";
import { CommonSection } from "../components/UI/common-section/CommonSection";
import { Shipping } from "../utils/interfaces/Shipping";

import "../styles/product-detail.css";
import "../styles/product-car.css";
import "../styles/checkout.css";

const Checkout = (): JSX.Element => {
  const [enterName, setEnterName] = useState<string>("");
  const [enterEmail, setEnterEmail] = useState<string>("");
  const [enterNumber, setEnterNumber] = useState<number>();
  const [enterCountry, setEnterCountry] = useState<string>("");
  const [enterCity, setEnterCity] = useState<string>("");
  const [enterPostal, setEnterPostal] = useState<string>("");
  const shippingInfo: Shipping[] = [];
  const cartTotalAmount = useAppSelector((state) => state.cart.totalAmount);
  const shippngCost = 30;

  const totalAmount = cartTotalAmount + Number(shippngCost);

  const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const userShippingAddress = {
      name: enterName,
      email: enterEmail,
      phone: enterNumber ?? 0,
      country: enterCountry,
      city: enterCity,
      postal: enterPostal,
    };
    shippingInfo.push(userShippingAddress);
    console.log(shippingInfo);
  };
  return (
    <Helmet title="Checkout">
      <CommonSection title="Checkout" />
      <section>
        <Container>
          <Row>
            <Col lg="8" md="6">
              <h5 className="mb-4">Shipping Address</h5>
              <form className="check__form" onSubmit={submitHandler}>
                <div className="form__group">
                  <input
                    type="text"
                    placeholder="Enter your name"
                    required
                    onChange={(e) => setEnterName(e.target.value)}
                  ></input>
                </div>
                <div className="form__group">
                  <input
                    type="email"
                    placeholder="Enter your email"
                    required
                    onChange={(e) => setEnterEmail(e.target.value)}
                  ></input>
                </div>
                <div className="form__group">
                  <input
                    type="number"
                    placeholder="Phone number"
                    required
                    onChange={(e) => setEnterNumber(Number(e.target.value))}
                  ></input>
                </div>
                <div className="form__group">
                  <input
                    type="text"
                    placeholder="Country"
                    required
                    onChange={(e) => setEnterCountry(e.target.value)}
                  ></input>
                </div>
                <div className="form__group">
                  <input
                    type="text"
                    placeholder="City"
                    required
                    onChange={(e) => setEnterCity(e.target.value)}
                  ></input>
                </div>
                <div className="form__group">
                  <input
                    type="number"
                    placeholder="Postal Code"
                    required
                    onChange={(e) => setEnterPostal(e.target.value)}
                  ></input>
                </div>
                <button className="addTOCart__btn">Payment</button>
              </form>
            </Col>
            <Col lg="4" md="6">
              <div className="checkout__bill">
                <h6 className="d-flex align-content-center justify-content-between mb-3">
                  Subtotal: $ <span>{totalAmount}</span>
                </h6>
                <h6 className="d-flex align-content-center justify-content-between mb-3">
                  Shipping: $<span>{shippngCost}</span>
                </h6>
                <div className="checkout__total">
                  <h5 className="d-flex align-content-center justify-content-between">
                    Total: <span>{totalAmount}</span>
                  </h5>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  );
};

export default Checkout;
