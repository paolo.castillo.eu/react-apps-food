import { useEffect, useState } from "react";
import Helmet from "../components/Helmet/Helmet";
import { Col, Container, Row, ListGroup, ListGroupItem } from "reactstrap";
import { Link } from "react-router-dom";
import { Category } from "../components/UI/category/Category";
import { ProductCard } from "../components/UI/product-card/ProductCard";
import { Button } from "../components/UI/button/Button";
import "../styles/hero-section.css";
import "../styles/home.css";
import hero_img from "../assets/images/hero.png";
import whyImg from "../assets/images/location.png";
import netWorking from "../assets/images/network.png";

import Products from "../assets/fake-data/Products";

import { IProducts } from "../utils/interfaces/Product.interfaces";
import { ICategory } from "../utils/interfaces/Category.interfaces";
import { POPULAR_DATA } from "../utils/interfaces/Popula.interfaces";
import { FEATURE_DATA } from "../utils/interfaces/Feature.interfaces";
import { Testimonial } from "../components/UI/slider/Testimonial";

const Home = (): JSX.Element => {
  const [category, setCategory] = useState<string>("ALL");
  const [products, setProduct] = useState<IProducts[]>(Products);
  const [hotPizza, setHotPizza] = useState<IProducts[]>([]);
  useEffect(() => {
    const filteredPizza = Products.filter((item) => item.category === "Pizza");
    const slicePizza = filteredPizza.slice(0, 4);
    setHotPizza(slicePizza);
  }, []);
  useEffect(() => {
    if (category === "ALL") {
      setProduct(Products);
    } else {
      const value = ICategory[category];
      const filteredProducts = Products.filter(
        (item) => item.category === value
      );
      setProduct(filteredProducts.length ? filteredProducts : Products);
    }
  }, [category]);
  console.log(category);
  return (
    <Helmet title="Home">
      <section>
        <Container>
          <Row>
            <Col lg="6" md="6">
              <div className="hero__content">
                <h5 className="mb-3">Easy way to make an order</h5>
                <h1 className="mb-4 hero__title">
                  <span>HUNGRY?</span> Just wait at food <span> you door</span>
                </h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Impedit officia accusantium consequuntur atque quam beatae
                  commodi iusto. Expedita, dolorum, quo neque sit ab perferendis
                  ipsa accusamus iusto architecto aliquid libero?
                </p>

                <div className="hero__btns d-flex align-content-center gap-5 mt-4">
                  <button className="order__btn d-flex align-items-center justify-content-between">
                    Order now <i className="ri-arrow-right-s-line"></i>
                  </button>
                  <button className="all_food-btn">
                    <Link to="/foods">See all foods</Link>
                  </button>
                </div>
                <div className="herro__service d-flex align-items-center gap-5 mt-5">
                  <p className="d-flex align-items-center gap-2">
                    <span className="shipping__icon">
                      <i className="ri-car-line"></i>
                    </span>{" "}
                    No shipping charge
                  </p>
                  <p className="d-flex align-items-center gap-2">
                    <span className="shipping__icon">
                      <i className="ri-shield-check-line"></i>
                    </span>{" "}
                    100% secure checkout
                  </p>
                </div>
              </div>
            </Col>
            <Col lg="6" md="6">
              <div className="hero__img">
                <img src={hero_img} alt="hero-img" className="w-100" />
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <section className="pt-0">
        <Category />
      </section>

      <section>
        <Container>
          <Row>
            <Col lg="12" className="text-center">
              <h5 className="feature__subtitle mb-4">What we serve</h5>
              <h2 className="feature__title">Just sit back at home</h2>
              <h2 className="feature__title">
                we will <span>take care</span>
              </h2>
              <p className="mb-1 mt-4 feature__text">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam
                iure maxime facilis architecto inventore ullam, rem, quibusdam
                nihil reprehenderit odit, voluptatem veritatis atque soluta?
                Porro incidunt architecto alias suscipit cumque.
              </p>
            </Col>
            {FEATURE_DATA.map((item, index) => (
              <Col lg="4" md="6" sm="6" xs="6" key={index} className="mt-5">
                <div className="feature__item text-center p-4 py-3">
                  <img
                    src={item.imgUrl}
                    alt="feature-img"
                    className="w-25 mb-3"
                  />
                  <h5 className="fw-bold mb-3">{item.title}</h5>
                  <p>{item.desc}</p>
                </div>
              </Col>
            ))}
          </Row>
        </Container>
      </section>
      <section>
        <Container>
          <Row>
            <Col lg="12" className="text-center">
              <h2>Popular Foods</h2>
            </Col>
            <Col lg="12">
              <div className="food__category d-flex align-content-center justify-content-center gap-4">
                {POPULAR_DATA.map((item, index) => (
                  <Button
                    key={index}
                    className={
                      item.className +
                      ` ${category === item.category ? "foodBtnActive" : ""}`
                    }
                    onClick={() => setCategory(item.category)}
                  >
                    {typeof item.imgUrl === undefined ? (
                      <img src={item.imgUrl} alt="" />
                    ) : (
                      ""
                    )}
                    {item.title}
                  </Button>
                ))}
              </div>
            </Col>
            {products.map((item, index) => (
              <Col lg="3" md="4" sm="6" xs="6" key={index} className="mt-5">
                <ProductCard item={item} />
              </Col>
            ))}
          </Row>
        </Container>
      </section>

      <section className="why__choose-us">
        <Container>
          <Row>
            <Col lg="" md="">
              <img src={whyImg} alt="why-tasty-treat" className="w-100" />
            </Col>
            <Col lg="" md="">
              <div className="why__tasty-treat">
                <h2 className="tasty__treat-title mb-4">
                  Why <span>Tasty Treat?</span>
                </h2>
                <p className="tasty__treat-desc">
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Veritatis, saepe! Animi incidunt dignissimos labore sit
                  nesciunt veniam cupiditate vitae laudantium. Omnis, laboriosam
                  libero! Nemo expedita rem tenetur possimus sit exercitationem.
                </p>
                <ListGroup className="m-4">
                  <ListGroupItem className="border-0 ps-0">
                    <p className="choose__us-title d-flex align-content-center gap-2">
                      <i className="ri-checkbox-circle-fill"></i>
                      Fresh and tasty foods
                    </p>
                    <p className="choose__us-desc">
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                      Earum qui, nisi, nulla omnis soluta doloremque ipsum
                      praesentium rerum nam, porro commodi officiis illo cumque
                      non! Reprehenderit enim voluptate fuga quisquam!
                    </p>
                  </ListGroupItem>
                  <ListGroupItem className="border-0 ps-0">
                    <p className="choose__us-title d-flex align-content-center gap-2">
                      <i className="ri-checkbox-circle-fill"></i>
                      Quality support foods
                    </p>
                    <p className="choose__us-desc">
                      Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Expedita ullam dolorum consectetur tenetur sunt voluptates
                      adipisci nemo molestias magni est nam, ipsum impedit ipsam
                      maxime atque, recusandae incidunt modi possimus!
                    </p>
                  </ListGroupItem>
                  <ListGroupItem className="border-0 ps-0">
                    <p className="choose__us-title d-flex align-content-center gap-2">
                      <i className="ri-checkbox-circle-fill"></i>
                      Order from any location
                    </p>
                    <p className="choose__us-desc">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Perferendis praesentium enim veritatis commodi suscipit,
                      error quod vitae eum quis cum totam temporibus nisi nam.
                      In id sapiente voluptatibus iste! Beatae!
                    </p>
                  </ListGroupItem>
                </ListGroup>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <section className="pt-0">
        <Container>
          <Row>
            <Col lg="12" className="text-center mb-5">
              <h2>hot Pizza</h2>
            </Col>
            {hotPizza.map((item) => (
              <Col lg="3" md="4" key={item.id}>
                <ProductCard item={item} />
              </Col>
            ))}
          </Row>
        </Container>
      </section>
      <section>
        <Container>
          <Row>
            <Col lg="6" md="6">
              <div className="testimonial">
                <h5 className="testimonial__subtitle mb-4">Testimonial</h5>
                <h2 className="testimonial__title mb-4">
                  What our <span>customers</span> are saying
                </h2>
                <p className="testimonial__desc">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Consequuntur iusto architecto voluptates, maiores hic beatae
                  quo quam quae non magnam facere sint adipisci fugiat eum eaque
                  quos necessitatibus delectus alias.
                </p>
                <Testimonial />
              </div>
            </Col>
            <Col lg="6" md="6">
              <img src={netWorking} alt="testimonial-img" className="w-100" />
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  );
};

export default Home;
