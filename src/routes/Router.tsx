import { lazy, Suspense } from "react";
import { Routes, Route } from "react-router-dom";
// import Home from "../pages/Home";
// import AllFood from "../pages/AllFood";
// import FoodDetails from "../pages/FoodDetails";
// import Cart from "../pages/Cart";
// import Checkout from "../pages/Checkout";
// import Contact from "../pages/Contact";
// import Login from "../pages/Login";
// import Register from "../pages/Register";
import { IRouter } from "../utils/interfaces/Router.interfaces";
import { ConfigRouter } from "./Config.router";
const Page404 = lazy(() => import("../pages/NotFound"));

const Routers = (): JSX.Element => {
  const renderRoutes = (routes: IRouter[], contextPath: string) => {
    const children: JSX.Element[] = [];
    const renderRoute = (item: IRouter, routeContextPath: string) => {
      let newContextPath = item.path
        ? `${routeContextPath}/${item.path}`
        : routeContextPath;
      newContextPath = newContextPath.replace(/\/+/g, "/");

      if (item.childRoutes) {
        const childRoutes = renderRoutes(item.childRoutes, newContextPath);
        children.push(<Route key={newContextPath}>{childRoutes}</Route>);
        item.childRoutes.forEach((r) => renderRoute(r, newContextPath));
      }

      children.push(
        <Route
          key={newContextPath}
          element={<item.component />}
          path={newContextPath}
        />
      );
    };
    routes.forEach((item) => renderRoute(item, contextPath));
    return children;
  };

  const routes = renderRoutes(ConfigRouter, "/");
  console.log(routes);
  return (
    <Suspense fallback={<h2>Cargando...</h2>}>
      <Routes>
        {routes}
        {/* <Route path="/" element={<Navigate to={"/home"} />} />
      <Route path="/home" element={<Home />} />
      <Route path="/foods" element={<AllFood />} />
      <Route path="/foods/:id" element={<FoodDetails />} />
      <Route path="/cart" element={<Cart />} />
      <Route path="/checkout" element={<Checkout />} />
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/contact" element={<Contact />} /> */}
        <Route element={<Page404 />} />
      </Routes>
    </Suspense>
  );
};

export default Routers;
