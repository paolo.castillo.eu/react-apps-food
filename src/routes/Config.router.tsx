import { lazy } from "react";
import { IRouter } from "../utils/interfaces/Router.interfaces";

const home = lazy(() => import("../pages/Home"));
const allFood = lazy(() => import("../pages/AllFood"));
const foodDetails = lazy(() => import("../pages/FoodDetails"));
const cart = lazy(() => import("../pages/Cart"));
const checkout = lazy(() => import("../pages/Checkout"));
const contact = lazy(() => import("../pages/Contact"));
const login = lazy(() => import("../pages/Login"));
const register = lazy(() => import("../pages/Register"));

const users: IRouter = {
  path: "/",
  name: "home",
  component: home,
  childRoutes: [
    {
      path: "home",
      component: home,
    },
    {
      path: "foods",
      component: allFood,
    },
    {
      path: "foods/:id",
      component: foodDetails,
    },
    {
      path: "cart",
      component: cart,
    },
    {
      path: "checkout",
      component: checkout,
    },
    {
      path: "login",
      component: login,
    },
    {
      path: "register",
      component: register,
    },
    {
      path: "contact",
      component: contact,
    },
  ],
};

export const ConfigRouter = [users];
