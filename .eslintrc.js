module.exports = {
  "env": {
      "browser": false,
      "es6": true,
      "node": true
  },
  "root": true,
  "extends": [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'prettier'
  ],
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
      "project": "tsconfig.json",
      "sourceType": "module"
  },
  "plugins": [
      "@typescript-eslint/eslint-plugin",
      "prettier"
  ],
  "rules": {
      "prettier/prettier": "error",
      "@typescript-eslint/no-unsafe-call": "off"
  }
};
